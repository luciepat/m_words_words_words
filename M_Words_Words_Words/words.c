#include "words.h"
/*
gets amount of letters in the word for of numbers in range
in: range
out: letter amount
*/
size_t num_words(size_t range)
{
	if (range > MAX_RANGE) {
		return 0;
	}
	char* numbers[MAX_RANGE] = { 0 };
	size_t num = 0;
	for (size_t i = 1; i <= range; i++) {
		numbers[i-1] = num_to_word(i);
	}
	for (size_t i = 0; i < range; i++) {
		for (size_t j = 0; j < strlen(numbers[i]); j++) {
			if ((ASCII_A <= numbers[i][j] && numbers[i][j] < ASCII_Z) || (ASCII_a <= numbers[i][j] && numbers[i][j] < ASCII_z)) {
				num++;
			}
		}
	}
	return num;
}
/*
turns nukber into word form 
in: num
out: pointer to word form
*/
char * num_to_word(size_t num)
{
	char* temp = NULL;
	char tt[50] = {0};
	char* twenty[20] = {"Zero", "One", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine", "Ten",
	"Eleven", "Twelve", "Thirteen", "Fourteen", "Fifteen", "Sixteen", "Seventeen", "Eighteen", "Nineteen" };
	char* tens[8] = { "Twenty", "Thirty", "Fourty", "Fifty", "Sixty", "Seventy","Eighty", "Ninety" };
	if (num > 0 && num < 20) { // no consistent pattern until 20 so just have all ready
		temp = (char*)malloc(sizeof(char)*(strlen(twenty[num] + 1)));
		if (!temp) {
			return NULL; // use to fail
		}
		strcpy_s(temp, strlen(twenty[num]) + 1, twenty[num]);
	}
	else if (num < MAX_RANGE){ //  between 20 and 100 there is a constitent pattern
		size_t tens_d;
		size_t ones;
		size_t length;
		ones = num % 10; // get singles digit
		num = num / 10;
		tens_d = num % 10; // get tens digit
		// below- length of tens digit + 1 + (0 if singles is 0 and length of single +1 otherwise)
		length = (ones > 0 ? strlen(twenty[ones]) +1 : 0) + strlen(tens[tens_d - 2]) + 1; //+2 (1-NULL+ 1-Space)
		temp = (char*)malloc(sizeof(char)*(length)); 
		if (!temp) {
			return NULL; // use to fail
		}
		strcpy_s(temp, length, tens[tens_d-2]);
		if (ones) {
			strcat_s(temp, length, " ");
			strcat_s(temp, length, twenty[ones]);
		}
	}
	else {
		temp = (char*)malloc(sizeof(char)*HUNDRED_LENGTH); // +2 (1-NULL+ 1-Space)
		if (!temp) {
			return NULL; // use to fail
		}
		strcpy_s(temp, sizeof(char) * HUNDRED_LENGTH, "One Hundred");
	}
	return temp;
}
