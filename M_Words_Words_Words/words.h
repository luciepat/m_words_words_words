#pragma once
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#define MAX_RANGE 100
#define HUNDRED_LENGTH 12
#define ASCII_A 65
#define ASCII_Z 92
#define ASCII_a 97
#define ASCII_z 122
size_t num_words(size_t range);
char* num_to_word(size_t num);